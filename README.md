# Shell Scripts     

This repository contains various shell scripts for various use. These scripts are being put in this repository so that they can be accessed and used by needing projects, so all projects will use the latest, updated scripts.     

## CPP Folder Scripts    
Currently, the scripts in this folder are about getting, and setting up (compiling) dependencies needed by projects. The script available can setup the following dependencies :     

* Assimp
* CGAL         
* Eigen     
* EMSDK    
* FreeGLUT      
* GLAD      
* GLFW     
* GLM      
* IfcPlusPlus     
* json     
* libigl     
* LuxCore     
* Mio     
* OpenCASCADE     
* OpenSceneGraph     
* PCL     
* uSockets     
* uWebSockets     


There might be two versions of the scripts, the standard and the dev version. The standard will get the release source code of the project, while the dev will take last version of the development code from the repository.           

## Docker Scripts    
The scripts in this folder are intended to prepare docker container (currently it will use Ubuntu image), get the source code of the projects as well as the dependencies, prepare all the tools and compiler needed, and finally compile all of the projects and dependencies.         

## VM Scripts      
The scripts in this folder are intended to prepare Virtual Machine (runnable by Virtual Box), with the project and dependencies inside.

## Questions?      
If you have any question, request, etc., please don't hesitate to contact me using LINE. Thank you.
