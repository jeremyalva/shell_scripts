echo "Setting Up Clash Detection Tool (CDT) Docker Container"
echo ""

echo "Getting ubuntu docker image."
docker pull ubuntu
echo ""

echo "Running ubuntu docker image."
docker run --name cdt -it ubuntu:latest bash
echo ""

echo "Updating system."
apt update
apt upgrade
echo ""

echo "Installing tools."
apt install build-essential
echo ""

echo "Installing CMake."
sudo apt install cmake
echo ""

echo "Installing OpenMP."
sudo apt install libomp-dev
echo ""

echo "Installing GNU M4."
sudo apt install m4
echo ""
# Note : this is needed by GNU GMP.

echo "Installing X11 Development."
sudo apt install libx11-dev
echo ""

echo "Installing GLFW Dependencies."
sudo apt install libxcursor-dev libxrandr-dev libxinerama-dev libxi-dev
echo ""

#echo "Installing CMake GUI"
#sudo apt install cmake-qt-gui
#echo ""

echo "Setting up and entering working directory."
cd "$(xdg-user-dir)"
mkdir workspace
cd workspace
echo ""

#echo "Getting Shell Scripts"
#wget -c https://gitlab.com/jeremyalva/shell_scripts/-/archive/2021.1.3_13.07/shell_scripts-2021.1.3_13.07.zip
#rm -rf shell_scripts-2021.1.3_13.07
#rm -rf shell_scripts

#echo "Extracting the shell scripts package."
#unzip -q -o shell_scripts-2021.1.3_13.07.zip
#mv shell_scripts-2021.1.3_13.07 shell_scripts
#echo 'Shell scripts are ready.'
#echo ""




echo ""

echo "Getting CDT."
#git clone
echo ""

echo "Compiling CDT dependencies and modules."
echo ""

echo "Completed."
