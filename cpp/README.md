# CPP Scripts     
The scripts in this folder are for building projects. There are two types of scripts, the sh and the lib. The sh files are can be run directly (as it is a shell script by itself), but the lib files are intended to be included in other shell script.      


#### setup_assimp.sh     
Download and compile Assimp v5.0.1. Source will be obtained from Assimp github repository https://github.com/assimp/assimp. Link to the source of assimp version 5.0.1 is here : https://github.com/assimp/assimp/archive/v5.0.1.zip

#### setup_eigen.sh     
Download and compile Eigen version 3.3.9 from its gitlab repository https://gitlab.com/libeigen/eigen. The link to v3.3.9 release source code is : https://gitlab.com/libeigen/eigen/-/archive/3.3.9/eigen-3.3.9.zip     

#### setup_emsdk.sh     
Setup EMSDK. We will not compile things from source, but rather get the compiled version from the online repository, and set it up on local system.

#### setup_ifcplusplus.sh     
Download and compile ifcplusplus. The github repository is https://github.com/ifcquery/ifcplusplus. The link to version 1.0 release is https://github.com/ifcquery/ifcplusplus/archive/1.0.zip

#### setup json.sh     
Download and compile json form its github repository https://github.com/nlohmann/json. The link to version 3.9.1 source code is https://github.com/nlohmann/json/archive/v3.9.1.zip     

#### setup_libigl.sh     
Download and compile libigl from its github repository https://github.com/libigl/libigl. The link to version 2.2.0 source code is https://github.com/libigl/libigl/archive/v2.2.0.zip     

#### setup luxcore.sh     
under development.     

#### setup_mio_dev.sh     
There is no release yet for this library, so we will get the latest development source code from its repository : https://github.com/mandreyel/mio     

#### setup_opencascade.sh     
Download and compile OpenCASCADE 7.5.0. Source will be obtained from OpenCASCADE Community Github repository https://github.com/tpaviot/oce. Link of the version 7.5.0 source is : https://github.com/tpaviot/oce/releases/download/official-upstream-packages/opencascade-7.5.0.tgz

#### setup_openscenegraph.sh     
Download and compile OpenSceneGraph 3.6.5. Source will be obtained from OpenSceneGraph github repository https://github.com/openscenegraph/OpenSceneGraph. Link of the v3.6.5 release is : https://github.com/openscenegraph/OpenSceneGraph/archive/OpenSceneGraph-3.6.5.zip      

#### setup_pcl.sh     
Download and compile PCL from its github repository https://github.com/PointCloudLibrary/pcl. The link to version 1.11.1 source code is https://github.com/PointCloudLibrary/pcl/archive/pcl-1.11.1.zip     

#### setup_usockets.sh      
Download and compile usockets from its github repository https://github.com/uNetworking/uSockets. The link to the version 0.6.0 source code is : https://github.com/uNetworking/uSockets/archive/v0.6.0.zip     

#### setup_uwebsockets.sh     
Download and compile uwebsockets from its github repository https://github.com/uNetworking/uWebSockets. The link to version 18.17 release is https://github.com/uNetworking/uWebSockets/archive/v18.17.0.zip
