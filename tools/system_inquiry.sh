echo "Detecting the CPU."
threads_count=$(grep -c processor /proc/cpuinfo)
echo "CPU threads : $threads_count"
echo ""

echo "System Inquiry."
hostnamectl
echo ""

echo "CPU Inquiry."
lscpu
echo ""

echo "Memory Inquiry"
lsmem
echo ""
