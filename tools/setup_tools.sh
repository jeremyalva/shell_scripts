echo "Will install the tools needed to build things."

echo "Updating and Upgrading this system."
sudo apt -q update
sudo apt -q upgrade
echo ""

echo "Installing wget."
sudo apt -q install wget
echo ""

echo "Installing cmake."
sudo apt -q install cmake
echo ""

echo "Installing zip unzip."
sudo apt -q install zip unzip
echo ""

echo "Installing git."
sudo apt -q install git
echo ""

echo "Installing build-essential."
sudo apt -q install build-essential
echo ""

echo "Installation of build tools completed."
